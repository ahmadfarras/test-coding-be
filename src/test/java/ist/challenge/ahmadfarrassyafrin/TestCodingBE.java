package ist.challenge.ahmadfarrassyafrin;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class TestCodingBE {
    List<List<Integer>> vouchers = new ArrayList<>();
    List<Integer> voucher = new ArrayList<>();

    @BeforeEach
    void setup(){
        voucher.add(10);
        voucher.add(100);
        vouchers.add(voucher);

        voucher = new ArrayList<>();
        voucher.add(25);
        voucher.add(200);
        vouchers.add(voucher);

        voucher = new ArrayList<>();
        voucher.add(50);
        voucher.add(400);
        vouchers.add(voucher);

        voucher = new ArrayList<>();
        voucher.add(100);
        voucher.add(800);
        vouchers.add(voucher);
    }

    void checkBiggestPointVoucher(List<List<Integer>> vouchers){
        Integer biggestPoint = 0, biggestVoucher = 0;
        for (List<Integer> voucher:vouchers) {
            if (biggestPoint < voucher.get(1)){
                biggestPoint = voucher.get(1);
                biggestVoucher = voucher.get(0);
            }
        }

        System.out.println("Voucher dengan point terbesar: " + biggestVoucher + "rb, Point: " + biggestPoint +"p");
    }


    void redeemAllPoint(List<List<Integer>> vouchers, Integer point){
        System.out.println("Poin Awal: " + point);

        StringBuilder getVoucher = new StringBuilder();

        Integer lowerPoint = 100000;
        String tempVoucher = "";
        for (List<Integer> voucher:vouchers) {
            if (lowerPoint > voucher.get(1)){
                lowerPoint = voucher.get(1);
            }
        }

        while(point > lowerPoint){
            int tempPoint = 0;
            for (List<Integer> voucher:vouchers) {
                if (point >= voucher.get(1)){
                    tempPoint = point - voucher.get(1);
                    tempVoucher = voucher.get(0) + "rb, ";
                }
            }
            point = tempPoint;
            getVoucher.append(tempVoucher);
        }


        stdPrintln(getVoucher, point);
    }

    void remainingPoint(List<List<Integer>> vouchers, Integer point){
        System.out.println("Poin Awal: " + point);

        StringBuilder getVoucher = new StringBuilder();
        int tempPoint = 0;

        for (List<Integer> voucher:vouchers) {
            if (point >= voucher.get(1)){
                tempPoint = point - voucher.get(1);
                getVoucher = new StringBuilder(voucher.get(0) + "rb");
            }
        }

        stdPrintln(getVoucher, tempPoint);
    }

    void stdPrintln(StringBuilder getVoucher, Integer point){
        System.out.println("Voucher yang didapat: " + getVoucher + "\nSisa Point: " + point +"p");
    }

    @Test
    void testBiggestVoucherPoint(){
        checkBiggestPointVoucher(vouchers);
        System.out.println("");
    }

    @Test
    void testRemainingPoints(){
        remainingPoint(vouchers, 700);
        System.out.println("");
    }

    @Test
    void testRedeemAllPoints(){
        redeemAllPoint(vouchers, 1150);
        System.out.println("");
    }
}
